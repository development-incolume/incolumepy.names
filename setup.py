# !/usr/bin/env python
# coding: utf-8
import incolumepy.names as names
from os.path import join
from setuptools import setup, find_packages


with open('README.rst') as readme_file:
    readme = readme_file.read()

with open(join('docs', 'CHANGES.rst')) as file:
    changes = file.read()

with open(join('docs', 'CONTRIBUTING.rst')) as file:
    contributing = file.read()

with open(join('docs', 'CONTRIBUTORS.rst')) as file:
    contributors = file.read()

setup(
    name=names.__title__,
    version=names.__version__,
    author=names.__author__,
    url="https://gitlab.com/development-incolume/names.git",
    description="Generate random names",
    long_description='\n\n'.join((
        readme,
        changes,
        contributing,
        contributors,
    )),
    license=names.__license__,
    packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
    package_data={
        'docs': ['*.rst'],
        'names': ['dist.*', 'version.txt'],
    },
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'names = names.main:main',
        ],
    },
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: Implementation :: PyPy',
    ],
    test_suite='nose.collector',
    tests_require='nose',
    install_requires=[
          # -*- Extra requirements: -*-
          'incolumepy.utils>=0.7.2'
      ],
)
